import { applyMiddleware, combineReducers, createStore } from 'redux'

import thunk from 'redux-thunk'

// actions.js
export const storeValue = val => ({
    type: 'STORE_VAL',
    val
})

// reducers.js
export const val = (state = {}, action) => {
    switch (action.type) {
        case 'STORE_VAL':
            return action.val
        default:
            return state
    }
}

export const reducers = combineReducers({
    val
})

// store.js
export function configureStore(initialState = {}) {
    const store = createStore(reducers, initialState, applyMiddleware(thunk))
    return store
}

export const store = configureStore()
