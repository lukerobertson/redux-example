const calcYear = val => _.round(val, 2).toLocaleString()
const calcMonth = val => _.round(val / 12, 2).toLocaleString()
const calcWeek = val => _.round(val / 52, 2).toLocaleString()
const calcDay = val => _.round(val / 261, 2).toLocaleString()
const calcHour = val => _.round(val / 261 / 8, 2).toLocaleString()

const calcTaxFree = val => {
    const taxFree = 11500
    const upperLimit = 100000
    const taxCalc = upperLimit - val
    const smallerCalc = taxFree - Math.abs(taxCalc) / 2

    if (taxCalc < 0) {
        return smallerCalc < 0 ? 0 : smallerCalc
    }

    return taxFree
}

const calcTax = val => {
    const allowance = 11500
    const higher = 45000
    const additional = 150000
    const intialVal = calcTaxFree(val) - val
    const additonalTax = 0.45
    const higherTax = 0.4
    const intialTax = 0.2
    let taxPaid = 0

    if (intialVal < 0) {
        if (val > additional) {
            taxPaid += (intialVal - additional) * additonalTax
            taxPaid += (additional - higher) * higherTax
            taxPaid += (higher - allowance) * intialTax
        } else if (val > higher) {
            taxPaid += intialVal * higherTax
            taxPaid += (higher - allowance) * intialTax
        } else {
            taxPaid += intialVal * intialTax
        }
    }

    return Math.abs(taxPaid)
}

const calcDeductible = val => (val - calcTaxFree(val) < 0 ? 0 : val - calcTaxFree(val))

const calcNi = val => {
    const allowance = 8164
    const higher = 45032
    const niFree = allowance - val
    const lowerNi = 0.12
    const higherNi = 0.02
    let niPaid = 0

    if (niFree < 0) {
        if (val > higher) {
            niPaid += (val - higher) * higherNi
            niPaid += (higher - allowance) * lowerNi
        } else if (val > allowance) {
            niPaid += (val - allowance) * lowerNi
        }
    }

    return niPaid
}

const calcStudent = val => {
    const allowance = 17775
    const taxFree = allowance - val

    return taxFree < 0 ? Math.abs(taxFree * 0.09) : 0
}

const calcNiClassTwo = val => {
    let total = 0
    const upperLimit = 45000
    const upperRate = 0.02
    const lowerLimit = 8164
    const lowerRate = 0.09

    if (val > upperLimit) {
        total += (val - upperLimit) * upperRate
        total += (upperLimit - lowerLimit) * lowerRate
    } else if (val > lowerLimit) {
        total += (val - lowerLimit) * lowerRate
    }

    if (val > 6025) {
        total += 148.2
    }

    return total
}

const totalDeductions = (val, student, classTwo) => {
    let total = classTwo ? calcTax(val) : calcTax(val) + calcNi(val)

    if (student) {
        total += calcStudent(val)
    }

    if (classTwo) {
        total += calcNiClassTwo(val)
    }

    return total
}

const totalEarnings = (val, student, classTwo) => val - totalDeductions(val, student, classTwo)

export {
    calcYear,
    calcMonth,
    calcWeek,
    calcDay,
    calcHour,
    calcTaxFree,
    calcTax,
    calcDeductible,
    calcNi,
    calcStudent,
    calcNiClassTwo,
    totalDeductions,
    totalEarnings
}
