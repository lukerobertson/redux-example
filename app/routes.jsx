import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'

import App from './App'
import Main from './components/Main/Main'

import { Provider } from 'react-redux';  
import { store } from './redux'

const browserHistory = createBrowserHistory()

const Routes = () => {
    return (
        <Provider store={store}>
            <Router history={browserHistory}>
                    <App>
                        <Route exact path="/" component={Main} />
                    </App>
            </Router>
        </Provider>
    )
}

export default Routes
