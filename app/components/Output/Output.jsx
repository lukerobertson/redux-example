import React, { Component } from 'react'

import { connect } from 'react-redux'

import { configureStore } from '../../redux'

import style from './Output.scss'

export const Output = props => {
    const { inputValue } = props.value

    return <div className="">{inputValue}</div>
}

Output.defaultProps = {
    value: { inputValue: 0 }
}

const mapStateToProps = (state, ownProps) => ({
    value: state.val
})

const conn = connect(mapStateToProps, {})(Output)

export default conn
