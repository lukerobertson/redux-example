import React, { Component } from 'react'

import { connect } from 'react-redux'

import { storeValue } from '../../redux'

import style from './Input.scss'

export const Input = props => (
    <div>
        <input type="number" onChange={e => props.storeValue({ inputValue: e.target.value })} />
    </div>
)

const mapStateToProps = (state, ownProps) => ({
    value: state.val
})

const mapDispatchToProps = {
    storeValue
}

const conn = connect(mapStateToProps, mapDispatchToProps)(Input)

export default conn
