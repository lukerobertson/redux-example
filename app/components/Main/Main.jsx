import React, { Component } from 'react'
import { connect } from 'redux'

import Input from '../Input/Input'
import Output from '../Output/Output'

import style from './Main.scss'

const Main = () => (
    <div className="main">
        <Input />
        <Output />
    </div>
)

export default Main
