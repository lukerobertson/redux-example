import React, { Component } from 'react'
import _ from 'lodash'

import css from './App.scss'

class App extends Component {
    state = {}

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}

export default App
